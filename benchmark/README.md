Style Benchmark
===============

This directory contains a benchmark that will benchmark two prominent
properties of QML styles: Item creation time and render speed. It tests these
across several builtin styles (Default, Material, Universal and Imagine) and
two KDE styles, qqc2-desktop-style and the Plasma style.

Results
-------

These are the results from running this on a Ryzen 2500U with AMD Vega GPU, Qt
5.15, No Scaling:

### Creation Time (average ms per iteration, 10000 iterations)

| Test         | Default | Material | Universal | Imagine | Desktop | Plasma  | Unified |
|--------------|---------|----------|-----------|---------|---------|---------|---------|
| Button       |  0.1199 |   0.5030 |    0.1524 |  0.2389 |  0.3597 |  2.0975 |  0.1454 |
| ItemDelegate |  0.1087 |   0.1670 |    0.1406 |  0.2472 |  0.2802 |  0.9012 |         |
| CheckBox     |  0.1617 |   0.2361 |    0.2099 |  0.2379 |  0.6592 |  1.6171 |         |
| RadioButton  |  0.1401 |   0.2424 |    0.1693 |  0.2404 |  0.6091 |  0.6328 |         |
| Label        |  0.0655 |   0.0824 |    0.0791 |  0.1376 |  0.2295 |  0.1095 |         |
| ProgressBar  |  0.0676 |   0.0878 |    0.0791 |  0.5079 |  0.1408 |  0.7181 |         |
| RangeSlider  |  0.1255 |   0.1824 |    0.1469 |  0.3201 |  0.2980 |  1.4722 |         |
| TextField    |  0.1145 |   0.1550 |    0.1695 |  0.2093 |  0.4713 |  1.6006 |  0.1756 |

### Frame Time (ms)

| Test         | Default | Material | Universal | Imagine | Desktop | Plasma  | Unified |
|--------------|---------|----------|-----------|---------|---------|---------|---------|
| Move         | 10.2881 |  10.2881 |    10.352 | 10.4167 | 10.4384 | 10.5042 | 11.3636 |
| Opacity      | 10.2459 |  10.2669 |    10.352 | 10.4167 | 10.4167 | 10.5042 | 10.917  |
| Width        | 10.2881 |  10.5932 |    10.395 | 10.4603 | 10.8460 | 11.3895 | 10.917  |
| Width/Height | 10.2881 |  10.6383 |    10.395 | 10.4603 | 11.9048 | 14.6199 | 11.0865 |
|              |         |          |           |         |         |         |
| Combined     | 12.6904 |  12.5628 |    12.500 | 12.7226 | 11.2613 | 13.8504 |

<!--
kate: word-wrap-column 100
-->
