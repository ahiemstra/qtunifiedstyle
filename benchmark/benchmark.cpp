
#include <QtTest/QtTest>

#include <QQmlComponent>
#include <QQuickItem>
#include <QQuickView>
#include <QQuickStyle>
#include <QQmlEngine>

class Benchmark : public QObject
{
    Q_OBJECT
public:
    Benchmark();

private Q_SLOTS:
    void initTestCase();
    void initTestCase_data();
    void init();

    void benchmarkCreate_data();
    void benchmarkCreate();

    void benchmarkFrameTime_data();
    void benchmarkFrameTime();

    void benchmarkCombined();

    void cleanup();
    void cleanupTestCase();

private:

    std::unique_ptr<QQuickView> m_window;
};

QTEST_MAIN(Benchmark)

Benchmark::Benchmark()
{
    qputenv("QTEST_FUNCTION_TIMEOUT", "600000");
    qputenv("vblank_mode", "0");
}

void Benchmark::initTestCase()
{
}

void Benchmark::initTestCase_data()
{
    QTest::addColumn<QString>("style");

    QTest::newRow("Default Style") << QStringLiteral("default");
    QTest::newRow("Material Style") << QStringLiteral("material");
    QTest::newRow("Universal Style") << QStringLiteral("universal");
    QTest::newRow("Imagine Style") << QStringLiteral("imagine");
    QTest::newRow("Desktop Style") << QStringLiteral("org.kde.desktop");
    QTest::newRow("Plasma Style") << QStringLiteral("plasma");
}

void Benchmark::init()
{
    m_window = std::make_unique<QQuickView>();
    m_window->setWidth(800);
    m_window->setHeight(600);
    m_window->show();

    if (!QTest::qWaitForWindowExposed(m_window.get())) {
        QFAIL("Could not create window");
    }
}

void Benchmark::benchmarkCreate_data()
{
    QTest::addColumn<QString>("qml");

    auto header = QStringLiteral(""
        "import QtQuick 2.15\n"
        "import QtQuick.Controls 2.15\n"
        "\n"
    );

    QTest::newRow("Button") << header + QStringLiteral(""
        "Button {\n"
        "   width: 100\n"
        "   height: 100\n"
        "   text: \"Test\"\n"
        "}"
    );

    QTest::newRow("ItemDelegate") << header + QStringLiteral(""
        "ItemDelegate {\n"
        "   width: 400\n"
        "   text: \"Test\"\n"
        "}"
    );

    QTest::newRow("CheckBox") << header + QStringLiteral(""
        "CheckBox {\n"
        "   width: 400\n"
        "   text: \"Test\"\n"
        "}"
    );

    QTest::newRow("RadioButton") << header + QStringLiteral(""
        "RadioButton {\n"
        "   width: 400\n"
        "   text: \"Test\"\n"
        "}"
    );

    QTest::newRow("Label") << header + QStringLiteral(""
        "Label {\n"
        "   text: \"Test\"\n"
        "}"
    );

    QTest::newRow("ProgressBar") << header + QStringLiteral(""
        "ProgressBar {\n"
        "   width: 400\n"
        "   value: 0.5\n"
        "}"
    );

    QTest::newRow("RangeSlider") << header + QStringLiteral(""
        "RangeSlider {\n"
        "   width: 400\n"
        "}"
    );

    QTest::newRow("TextField") << header + QStringLiteral(""
        "TextField {\n"
        "   width: 400\n"
        "   text: \"Test\"\n"
        "}"
    );
}

void Benchmark::benchmarkCreate()
{
    QFETCH_GLOBAL(QString, style);
    QQuickStyle::setStyle(style);

    QFETCH(QString, qml);

    QQmlComponent component{m_window->engine(), m_window->rootObject()};
    component.setData(qml.toUtf8(), QUrl{});
    if (component.isError()) {
        for (auto error : component.errors()) {
            qWarning() << error.toString();
        }
        QFAIL("Could not create component");
    }

    QBENCHMARK {
        auto object = qobject_cast<QQuickItem*>(component.create(m_window->rootContext()));
        QVERIFY(object != nullptr);
        delete object;
    }
}

void Benchmark::benchmarkFrameTime_data()
{
    QTest::addColumn<QString>("qml");

    auto header = QStringLiteral(""
        "import QtQuick 2.15\n"
        "import QtQuick.Controls 2.15\n"
        "\n"
    );

    QTest::newRow("Move") << header + QStringLiteral(""
        "Item {\n"
        "   id: root\n"
        "   anchors.fill: parent\n"
        "   Button {\n"
        "       text: \"Test\"\n"
        "       NumberAnimation on x { from: 0; to: 800; duration: 1000; loops: Animation.Infinite; running: true }"
        "   }"
        "}"
    );

    QTest::newRow("Opacity") << header + QStringLiteral(""
        "Item {\n"
        "   id: root\n"
        "   anchors.fill: parent\n"
        "   Button {\n"
        "       text: \"Test\"\n"
        "       opacity: 0.0\n"
        "       NumberAnimation on opacity { from: 0; to: 1.0; duration: 1000; loops: Animation.Infinite; running: true }"
        "   }"
        "}"
    );

    QTest::newRow("Width") << header + QStringLiteral(""
        "Item {\n"
        "   id: root\n"
        "   anchors.fill: parent\n"
        "   Button {\n"
        "       text: \"Test\"\n"
        "       NumberAnimation on width { from: 0; to: 800; duration: 1000; loops: Animation.Infinite; running: true }"
        "   }"
        "}"
    );

    QTest::newRow("Width & Height") << header + QStringLiteral(""
        "Item {\n"
        "   id: root\n"
        "   anchors.fill: parent\n"
        "   Button {\n"
        "       text: \"Test\"\n"
        "       NumberAnimation on width { from: 0; to: 800; duration: 1000; loops: Animation.Infinite; running: true }"
        "       NumberAnimation on height { from: 0; to: 800; duration: 1000; loops: Animation.Infinite; running: true }"
        "   }"
        "}"
    );
}

void Benchmark::benchmarkFrameTime()
{
    QFETCH_GLOBAL(QString, style);
    QQuickStyle::setStyle(style);

    QFETCH(QString, qml);

    static const float duration = 5.0;
    static const float duration_ms = duration * 1000.0;

    QQmlComponent component{m_window->engine()};
    component.setData(qml.toUtf8(), QUrl{});
    if (component.isError()) {
        for (auto error : component.errors()) {
            qWarning() << error.toString();
        }
        QFAIL("Could not create component");
    }

    auto item = qobject_cast<QQuickItem*>(component.create(m_window->rootContext()));
    item->setParentItem(m_window->contentItem());

    QSignalSpy frameSwappedSpy{m_window.get(), &QQuickWindow::frameSwapped};

    QTest::qWaitFor([&frameSwappedSpy]() { return frameSwappedSpy.count() >= 1; });
    QTest::qWait(duration_ms);

    qDebug() << frameSwappedSpy.count() << "frames in" << duration << frameSwappedSpy.count() / duration << "FPS or" << duration_ms / frameSwappedSpy.count() << "ms per frame";
}

void Benchmark::benchmarkCombined()
{
    QFETCH_GLOBAL(QString, style);
    QQuickStyle::setStyle(style);

    auto qml = QStringLiteral(""
        "import QtQuick 2.15\n"
        "import QtQuick.Controls 2.15\n"
        ""
        "GridView {\n"
        "   id: root\n"
        "   anchors.fill: parent\n"
        "   model: 10000\n"
        "   cellWidth: 50\n"
        "   cellHeight: 50\n"
        "   delegate: ItemDelegate {\n"
        "       width: 50\n"
        "       height: 50\n"
        "       text: modelData\n"
        "   }\n"
        "   Timer {\n"
        "       interval: 500\n"
        "       running: true\n"
        "       repeat: true\n"
        "       triggeredOnStart: true\n"
        "       onTriggered: root.flick(0, -1000)\n"
        "   }\n"
        "}"
    );

    static const float duration = 5.0;
    static const float duration_ms = duration * 1000.0;

    QQmlComponent component{m_window->engine()};
    component.setData(qml.toUtf8(), QUrl{});
    if (component.isError()) {
        for (auto error : component.errors()) {
            qWarning() << error.toString();
        }
        QFAIL("Could not create component");
    }

    auto item = qobject_cast<QQuickItem*>(component.create(m_window->rootContext()));
    item->setParentItem(m_window->contentItem());

    QSignalSpy frameSwappedSpy{m_window.get(), &QQuickWindow::frameSwapped};

    QTest::qWaitFor([&frameSwappedSpy]() { return frameSwappedSpy.count() >= 1; });
    QTest::qWait(duration_ms);

    qDebug() << frameSwappedSpy.count() << "frames in" << duration << frameSwappedSpy.count() / duration << "FPS or" << duration_ms / frameSwappedSpy.count() << "ms per frame";
}

void Benchmark::cleanup()
{
    m_window.reset();

    qmlClearTypeRegistrations();
}

void Benchmark::cleanupTestCase()
{
}

#include "benchmark.moc"
