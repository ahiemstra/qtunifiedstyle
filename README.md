# QtUnifiedStyle

A Proof of Concept of a style that uses the same input to style both QtWidgets
and QtQuick Controls.

## Design

The style is separated into three parts: input, intermediate representation and
output. The general idea is that input format is decoupled from the output
format. The input format is parsed and converted to an "intermediate
representation", an object that exposes a common set of properties not
dependant on the input format. These intermediate objects are then used by the
output formats to represent the style as best as possible.

## Current State

Right now, four parts are implemented: A core intermediate representation, a
CSS based input, a QML output and a very rough Widgets output. The QML output
can style Button and TextField, the Widgets output only supports Button at the
moment. Only several properties are supported: color, background-color,
padding, border-radius, border, shadow.

## Requirements

It needs Qt 5.15, a C++17 capable compiler and the Katana CSS parser from
https://github.com/hackers-painters/katana-parser/ .
