/*
 * SPDX-FileCopyrightText: 2020 Arjen Hiemstra <ahiemstra@heimr.nl>
 * 
 * SPDX-License-Identifier: LGPL-2.1-only OR LGPL-3.0-only OR LicenseRef-KDE-Accepted-LGPL
 */

#ifndef CSSLOADER_H
#define CSSLOADER_H

#include <memory>
#include <QObject>

class StyleElement;

class Q_DECL_EXPORT CssLoader : public QObject
{
    Q_OBJECT

public:
    CssLoader(QObject* parent = nullptr);
    ~CssLoader() override;

    bool load(const QUrl &url);

    std::shared_ptr<StyleElement> get(const QString &elementName, const QString &subElementName);

private:
    class Private;
    const std::unique_ptr<Private> d;
};

#endif // CSSLOADER_H
