/*
 * SPDX-FileCopyrightText: 2020 Arjen Hiemstra <ahiemstra@heimr.nl>
 * 
 * SPDX-License-Identifier: LGPL-2.1-only OR LGPL-3.0-only OR LicenseRef-KDE-Accepted-LGPL
 */

#include "CssLoader.h"

#include <functional>
#include <unordered_map>

#include <QFile>
#include <QDebug>
#include <QUrl>
#include <QGuiApplication>
#include <QPalette>

#include <katana.h>

#include "StyleElement.h"
#include <unistd.h>

#define qs QStringLiteral

QColor parseColor(void* value)
{
    auto katanaValue = reinterpret_cast<KatanaValue*>(value);

    auto string = QString::fromLatin1(katanaValue->string);

    if (katanaValue->unit == KATANA_VALUE_PARSER_HEXCOLOR) {
        return QColor{qs("#") % string};
    }

    if (katanaValue->unit == KATANA_VALUE_IDENT) {
        if (string.startsWith(qs("palette"))) {
            auto palette = qGuiApp->palette();
            if (string == qs("palette-text")) {
                return palette.color(QPalette::Text);
            }
            if (string == qs("palette-background")) {
                return palette.color(QPalette::Base);
            }
            if (string == qs("palette-highlight")) {
                return palette.color(QPalette::Highlight);
            }
            if (string == qs("palette-separator")) {
                return palette.color(QPalette::Light);
            }
            if (string == qs("palette-disabled")) {
                return palette.color(QPalette::Disabled, QPalette::Base);
            }
            if (string == qs("palette-disabledtext")) {
                return palette.color(QPalette::Disabled, QPalette::Text);
            }
        }
    }

    return QColor{};
}

qreal parseSize(void* value)
{
    auto katanaValue = reinterpret_cast<KatanaValue*>(value);
    if (katanaValue->unit == KATANA_VALUE_PX) {
        return katanaValue->fValue;
    }

    return 0.0;
}

class CssLoader::Private
{
public:
    Private(CssLoader* qq) : q(qq) { }

    void updateElement(const QString &elementName, KatanaStyleRule *rule);
    void updateHover(const QString& elementName, KatanaStyleRule* rule);
    void updateFocus(const QString& elementName, KatanaStyleRule* rule);
    void updateActive(const QString& elementName, KatanaStyleRule* rule);
    void updateSubElement(const QString &elementName, const QString &subElementName, KatanaStyleRule *rule);
    void updateDeclarations(StyleElement::Ptr element, KatanaStyleRule* rule);

    CssLoader* q = nullptr;

    std::unordered_map<QString, StyleElement::Ptr> elements;

    static std::unordered_map<QString, std::function<void(StyleElement::Ptr, KatanaArray*)>> propertyHandlers;
};

std::unordered_map<QString, std::function<void(StyleElement::Ptr, KatanaArray*)>> CssLoader::Private::propertyHandlers;

CssLoader::CssLoader(QObject* parent)
    : QObject(parent)
    , d(std::make_unique<Private>(this))
{
    if (d->propertyHandlers.empty()) {
        d->propertyHandlers.emplace(qs("color"), [](StyleElement::Ptr element, KatanaArray* values) {
            element->m_color = parseColor(values->data[0]);
        });
        d->propertyHandlers.emplace(qs("background-color"), [](StyleElement::Ptr element, KatanaArray* values) {
            element->m_backgroundColor = parseColor(values->data[0]);
        });
        d->propertyHandlers.emplace(qs("width"), [](StyleElement::Ptr element, KatanaArray* values) {
            element->m_width = parseSize(values->data[0]);
        });
        d->propertyHandlers.emplace(qs("height"), [](StyleElement::Ptr element, KatanaArray* values) {
            element->m_height = parseSize(values->data[0]);
        });
        d->propertyHandlers.emplace(qs("padding-left"), [](StyleElement::Ptr element, KatanaArray* values) {
            element->m_leftPadding = parseSize(values->data[0]);
        });
        d->propertyHandlers.emplace(qs("padding-right"), [](StyleElement::Ptr element, KatanaArray* values) {
            element->m_rightPadding = parseSize(values->data[0]);
        });
        d->propertyHandlers.emplace(qs("padding-top"), [](StyleElement::Ptr element, KatanaArray* values) {
            element->m_topPadding = parseSize(values->data[0]);
        });
        d->propertyHandlers.emplace(qs("padding-bottom"), [](StyleElement::Ptr element, KatanaArray* values) {
            element->m_bottomPadding = parseSize(values->data[0]);
        });
        d->propertyHandlers.emplace(qs("padding"), [](StyleElement::Ptr element, KatanaArray* values) {
            if (values->length == 1) {
                auto size = parseSize(values->data[0]);
                element->m_leftPadding = size;
                element->m_rightPadding = size;
                element->m_topPadding = size;
                element->m_bottomPadding = size;
            } else if (values->length == 2) {
                auto verticalSize = parseSize(values->data[0]);
                auto horizontalSize = parseSize(values->data[1]);
                element->m_leftPadding = horizontalSize;
                element->m_rightPadding = horizontalSize;
                element->m_topPadding = verticalSize;
                element->m_bottomPadding = verticalSize;
            } else if (values->length == 3) {
                auto horizontalSize = parseSize(values->data[1]);
                element->m_leftPadding = horizontalSize;
                element->m_rightPadding = horizontalSize;
                element->m_topPadding = parseSize(values->data[0]);
                element->m_bottomPadding = parseSize(values->data[2]);
            } else {
                element->m_topPadding = parseSize(values->data[0]);
                element->m_rightPadding = parseSize(values->data[1]);
                element->m_bottomPadding = parseSize(values->data[2]);
                element->m_leftPadding = parseSize(values->data[3]);
            }
        });
        d->propertyHandlers.emplace(qs("border-color"), [](StyleElement::Ptr element, KatanaArray* values) {
            element->m_borderColor = parseColor(values->data[0]);
        });
        d->propertyHandlers.emplace(qs("border-width"), [](StyleElement::Ptr element, KatanaArray* values) {
            element->m_borderWidth = parseSize(values->data[0]);
        });
        d->propertyHandlers.emplace(qs("border"), [](StyleElement::Ptr element, KatanaArray* values) {
            if (values->length == 2) {
                element->m_borderWidth = parseSize(values->data[0]);
            }

            if (values->length == 3) {
                element->m_borderWidth = parseSize(values->data[0]);
                element->m_borderColor = parseColor(values->data[2]);
            }
        });
        d->propertyHandlers.emplace(qs("box-shadow"), [](StyleElement::Ptr element, KatanaArray* values) {
            if (values->length == 3) {
                element->m_shadowXOffset = parseSize(values->data[0]);
                element->m_shadowYOffset = parseSize(values->data[1]);
                element->m_shadowColor = parseColor(values->data[2]);
            } else if (values->length == 4) {
                element->m_shadowXOffset = parseSize(values->data[0]);
                element->m_shadowYOffset = parseSize(values->data[1]);
                element->m_shadowSize = parseSize(values->data[2]);
                element->m_shadowColor = parseColor(values->data[3]);
            }
        });
        d->propertyHandlers.emplace(qs("border-top-left-radius"), [](StyleElement::Ptr element, KatanaArray *values) {
            element->m_topLeftRadius = parseSize(values->data[0]);
        });
        d->propertyHandlers.emplace(qs("border-top-right-radius"), [](StyleElement::Ptr element, KatanaArray *values) {
            element->m_topRightRadius = parseSize(values->data[0]);
        });
        d->propertyHandlers.emplace(qs("border-bottom-left-radius"), [](StyleElement::Ptr element, KatanaArray *values) {
            element->m_bottomLeftRadius = parseSize(values->data[0]);
        });
        d->propertyHandlers.emplace(qs("border-bottom-right-radius"), [](StyleElement::Ptr element, KatanaArray *values) {
            element->m_bottomRightRadius = parseSize(values->data[0]);
        });
        d->propertyHandlers.emplace(qs("border-radius"), [](StyleElement::Ptr element, KatanaArray *values) {
            if (values->length == 1) {
                auto size = parseSize(values->data[0]);
                element->m_topLeftRadius = size;
                element->m_topRightRadius = size;
                element->m_bottomLeftRadius = size;
                element->m_bottomRightRadius = size;
            } else if (values->length == 2) {
                auto leftTopRightBottom = parseSize(values->data[0]);
                auto rightTopLeftBottom = parseSize(values->data[1]);
                element->m_topLeftRadius = leftTopRightBottom;
                element->m_topRightRadius = rightTopLeftBottom;
                element->m_bottomLeftRadius = rightTopLeftBottom;
                element->m_bottomRightRadius = leftTopRightBottom;
            } else if (values->length == 3) {
                auto rightTopLeftBottom = parseSize(values->data[1]);
                element->m_topLeftRadius = parseSize(values->data[0]);
                element->m_topRightRadius = rightTopLeftBottom;
                element->m_bottomLeftRadius = rightTopLeftBottom;
                element->m_bottomRightRadius = parseSize(values->data[2]);
            } else {
                element->m_topLeftRadius = parseSize(values->data[0]);
                element->m_topRightRadius = parseSize(values->data[1]);
                element->m_bottomRightRadius = parseSize(values->data[2]);
                element->m_bottomLeftRadius = parseSize(values->data[3]);
            }
        });
    }
}

CssLoader::~CssLoader()
{
}

bool CssLoader::load(const QUrl &url)
{
    FILE* file = fopen(url.toLocalFile().toUtf8().data(), "r");
    if (!file) {
        qWarning() << "Could not open file" << url;
        return false;
    }

    auto parsed = katana_parse_in(file);
    fclose(file);

    for (size_t e = 0; e < parsed->errors.length; ++e) {
        auto error = static_cast<KatanaError*>(parsed->errors.data[e]);
        qWarning() << "Line" << error->first_line << QByteArray(error->message, 100);
    }
    if (parsed->errors.length > 0) {
        return false;
    }

    auto sheet = parsed->stylesheet;

    for (size_t i = 0; i < sheet->rules.length; ++i) {
        auto rule = reinterpret_cast<KatanaRule*>(sheet->rules.data[i]);
        if (rule->type == KatanaRuleStyle) {
            auto styleRule = reinterpret_cast<KatanaStyleRule*>(rule);
            for (size_t s = 0; s < styleRule->selectors->length; ++s) {
                auto selector = reinterpret_cast<KatanaSelector*>(styleRule->selectors->data[s]);

                if (selector->match == KatanaSelectorMatchTag) {
                    auto tag = QString::fromLatin1(selector->tag->local);
                    d->updateElement(tag, styleRule);
                }

                if (selector->match == KatanaSelectorMatchPseudoClass) {
                    if (selector->tagHistory->match == KatanaSelectorMatchTag) {
                        auto tag = QString::fromLatin1(selector->tagHistory->tag->local);

                        switch(selector->pseudo) {
                            case KatanaPseudoActive:
                                d->updateSubElement(tag, qs("active"), styleRule);
                                break;
                            case KatanaPseudoFocus:
                                d->updateSubElement(tag, qs("focus"), styleRule);
                                break;
                            case KatanaPseudoHover:
                                d->updateSubElement(tag, qs("hover"), styleRule);
                                break;
                            default:
                                break;
                        }
                    }
                }

                if (selector->match == KatanaSelectorMatchPseudoElement) {
                    if (selector->tagHistory->match == KatanaSelectorMatchTag) {
                        auto tag = QString::fromLatin1(selector->tagHistory->tag->local);
                        auto element = QString::fromLatin1(selector->data->value);
                        d->updateSubElement(tag, element, styleRule);
                    }
                }
            }
        }
    }

    katana_destroy_output(parsed);

    return true;
}

StyleElement::Ptr CssLoader::get(const QString &elementName, const QString &subElementName)
{
    QString element = elementName;
    if (!subElementName.isEmpty()) {
        element = element + QLatin1Char(':') + subElementName;
    }

    if (d->elements.find(element) != d->elements.end()) {
        return d->elements[element];
    }

    return nullptr;
}

void CssLoader::Private::updateElement(const QString &elementName, KatanaStyleRule* rule)
{
    if (elements.find(elementName) == elements.end()) {
        elements.emplace(elementName, std::make_shared<StyleElement>(q));
    }

    updateDeclarations(elements[elementName], rule);
}

void CssLoader::Private::updateSubElement(const QString& elementName, const QString &subElementName, KatanaStyleRule* rule)
{
    QString fullElementName = elementName + QLatin1Char(':') + subElementName;

    if (elements.find(fullElementName) == elements.end()) {
        elements.emplace(fullElementName, std::make_shared<StyleElement>(q));
    }

    auto element = elements[fullElementName];
    if (auto itr = elements.find(elementName); itr != elements.end()) {
        element->m_inherit = itr->second;
    }

    updateDeclarations(element, rule);
}

void CssLoader::Private::updateDeclarations(StyleElement::Ptr element, KatanaStyleRule* rule)
{
    for (size_t d = 0; d < rule->declarations->length; ++d) {
        auto declaration = reinterpret_cast<KatanaDeclaration*>(rule->declarations->data[d]);
        auto property = QString::fromLatin1(declaration->property);

        if (auto itr = propertyHandlers.find(property); itr != propertyHandlers.end()) {
            itr->second(element, declaration->values);
        }
    }

    Q_EMIT element->updated();
}
