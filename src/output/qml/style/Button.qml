/*
    SPDX-FileCopyrightText: 2017 Marco Martin <mart@kde.org>
    SPDX-FileCopyrightText: 2017 The Qt Company Ltd.

    SPDX-License-Identifier: LGPL-3.0-only OR GPL-2.0-or-later
*/

import QtQuick 2.15
import QtQuick.Templates 2.15 as T

import org.kde.kirigami 2.12 as Kirigami

import org.kde.unifiedstyle 1.0 as Unified

T.Button {
    id: control

    implicitWidth: Math.max(background.implicitWidth, contentItem.implicitWidth + leftPadding + rightPadding)
    implicitHeight: Math.max(background.implicitHeight, contentItem.implicitHeight + topPadding + bottomPadding)

    hoverEnabled: true

    leftPadding: styleItem.style.leftPadding
    rightPadding: styleItem.style.rightPadding
    topPadding: styleItem.style.topPadding
    bottomPadding: styleItem.style.bottomPadding

    font: styleItem.font

    contentItem: Text {
        text: control.text
        font: control.font
        color: styleItem.style.color

        verticalAlignment: Qt.AlignVCenter
    }

    background: Kirigami.ShadowedRectangle {
        implicitWidth: styleItem.style.width
        implicitHeight: styleItem.style.height

        color: styleItem.style.backgroundColor

        corners.topLeftRadius: styleItem.style.topLeftRadius
        corners.topRightRadius: styleItem.style.topRightRadius
        corners.bottomLeftRadius: styleItem.style.bottomLeftRadius
        corners.bottomRightRadius: styleItem.style.bottomRightRadius

        border.width: styleItem.style.borderWidth
        border.color: styleItem.style.borderColor

        shadow.size: styleItem.style.shadowSize
        shadow.color: styleItem.style.shadowColor
        shadow.xOffset: styleItem.style.shadowXOffset
        shadow.yOffset: styleItem.style.shadowYOffset
    }

    Unified.StyleItem {
        id: styleItem
        elementName: "button"

        hover: control.hovered
        focus: control.focus
        active: control.down
    }
}

