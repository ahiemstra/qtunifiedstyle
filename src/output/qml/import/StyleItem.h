/*
 * SPDX-FileCopyrightText: 2020 Arjen Hiemstra <ahiemstra@heimr.nl>
 * 
 * SPDX-License-Identifier: LGPL-2.1-only OR LGPL-3.0-only OR LicenseRef-KDE-Accepted-LGPL
 */

#pragma once

#include <QQuickItem>

class StyleElement;

class StyleItem : public QQuickItem
{
    Q_OBJECT
    Q_PROPERTY(QString elementName READ elementName WRITE setElementName NOTIFY elementNameChanged)
    Q_PROPERTY(StyleElement *style READ styleElement NOTIFY styleElementChanged)

    Q_PROPERTY(bool hover READ hover WRITE setHover NOTIFY hoverChanged)
    Q_PROPERTY(bool focus READ focus WRITE setFocus NOTIFY focusChanged)
    Q_PROPERTY(bool active READ active WRITE setActive NOTIFY activeChanged)

public:
    StyleItem(QQuickItem *parent = nullptr);

    QString elementName() const;
    void setElementName(const QString &newElementName);
    Q_SIGNAL void elementNameChanged();

    StyleElement *styleElement() const;
    Q_SIGNAL void styleElementChanged();

    bool hover() const;
    void setHover(bool newHover);
    Q_SIGNAL void hoverChanged();

    bool focus() const;
    void setFocus(bool newFocus);
    Q_SIGNAL void focusChanged();

    bool active() const;
    void setActive(bool newActive);
    Q_SIGNAL void activeChanged();

protected:
    QSGNode *updatePaintNode(QSGNode *node, QQuickItem::UpdatePaintNodeData *data) override;

private:
    QString m_elementName;

    std::shared_ptr<StyleElement> m_element;
    std::shared_ptr<StyleElement> m_hoverElement;
    std::shared_ptr<StyleElement> m_focusElement;
    std::shared_ptr<StyleElement> m_activeElement;

    bool m_hover = false;
    bool m_focus = false;
    bool m_active = false;
};
